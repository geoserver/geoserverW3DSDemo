/*! Geoserver Annotation Addon - v1.0 - 2014/01/27
 *
 * Copyright (c) 2014 Ji-Sun Kim; Licensed ARC and CGIT*/
var init_called = false;
var nav_change = true;
var cur_bar_pos;
var annos = []; //place marker
var annos2D = []; //text annotation
var annoCnt = 0;
var cur_anno_idx = 0; //table's cur tr id relevant index
var cur_span_idx = 0; //span's id relevant index
var cur_image_url = "";
var href_img_id = 0;
// var runtime = null;
var textbox_counter = 0;
var max_annotation = 100;
var spantag = new Array(max_annotation);

var local_pos = new Create2DArray(max_annotation);
var world_pos = new Create2DArray(max_annotation);

var anno_mode = false;

var link;
var el;
var geocode_attempt = true;
var geocoded_address = "";

var map = null;
var init_min_x = 546970.349;
var init_min_y = 4115470.23;
var init_max_x = 554500.349;
var init_max_y = 4124725.23;

Proj4js.defs["EPSG:26917"] = "+proj=utm +zone=17 +ellps=GRS80 +datum=NAD83 +units=m +no_defs";
Proj4js.defs["EPSG:4326"] = "+proj=longlat +ellps=WGS84 +datum=WGS84 +units=degrees";
Proj4js.defs["EPSG:900913"] = "+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +no_defs";

//Proj4js
var source = new Proj4js.Proj('EPSG:26917');
var dest = new Proj4js.Proj('EPSG:4326');
var for_OSM = new Proj4js.Proj('EPSG:900913');

var min_x = init_min_x;
var min_y = init_min_y;
var max_x = init_max_x;
var max_y = init_max_y;

var extent_4326 = new OpenLayers.Bounds(min_x, min_y, max_x, max_y);
var extent_26917 = new OpenLayers.Bounds(min_x, min_y, max_x, max_y);
var extent_900913 = new OpenLayers.Bounds(min_x, min_y, max_x, max_y);
//var utm = "+proj=utm +zone=17";
//var wgs84 = "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs";

var dlg_name = "";
var dlg_datetime = "";
var dlg_note = "";
var dlg_url = "";
var dlg_address = "";
var hit_pos = new Array(3);
var zoomed = false;
//FullScreen
$(window).resize(function(){
    var isFullScreen = document.fullScreen || 
                   document.mozFullScreen || 
                   document.webkitIsFullScreen;


    if(!isFullScreen) {
        X3D_minimize();
        zoomed = false;
    }
});

function X3D_fullscreen() {
    var ifram_body = document.body;
    var x3delement = document.getElementById('element');
    var x3dcontainer = document.getElementById('container');
    var anno_mode_element = document.getElementById('annotation_mode');    

    var t = document.getElementById('toggle');
    t.style.bottom = "14px";
    t.innerHTML = "<img src='http://metagrid2.sv.vt.edu:8080/scripts/revertscreen.png' width='24' height='24' style='margin-top:1px;'>";

    x3delement.style.width = "100%";
    x3delement.style.height = "100%";
    x3dcontainer.style.display="default";
    anno_mode_element.style.right = "10px";

    if (ifram_body.requestFullScreen) {                
        ifram_body.requestFullScreen();
        //x3delement.requestFullScreen();
    } else if (ifram_body.mozRequestFullScreen) {
        //x3delement.mozRequestFullScreen();
        ifram_body.mozRequestFullScreen();
    } else if (ifram_body.webkitRequestFullScreen) {
        //x3delement.webkitRequestFullScreen();
        ifram_body.webkitRequestFullScreen();
    }
    
} 
function X3D_minimize() {
    var ifram_body = document.body;
    var x3delement = document.getElementById('element');
    var x3dcontainer = document.getElementById('container');
    var anno_mode_element = document.getElementById('annotation_mode');  
    
    x3delement.style.width = "800px";
    x3delement.style.height = "500px";
    x3dcontainer.style.display="inline-block";
    anno_mode_element.style.right = "310px";

    var t = document.getElementById('toggle');
    t.style.bottom = "0px";
    t.innerHTML = "<img src='http://metagrid2.sv.vt.edu:8080/scripts/fullscreen.jpg' width='24' height='24' style='margin-top:1px;'>"; 
    if (ifram_body.exitFullscreen) {
        ifram_body.exitFullscreen();
    }

    else if (ifram_body.mozCancelFullScreen) {
        ifram_body.mozCancelFullScreen();
    }

    else if (ifram_body.webkitCancelFullScreen) {
        ifram_body.webkitCancelFullScreen();
    }                 
}                    

function toggle_screen() {
    if (!zoomed) {
        X3D_fullscreen();
        
    }
    else {
         X3D_minimize();
         
    }
    zoomed = !zoomed;
}

function UTM_LatLon(utm_pos, source, dest) {
    var p = new Proj4js.Point(utm_pos[0], utm_pos[1]);
    Proj4js.transform(source, dest, p);
    utm_pos[0] = p.x;
    utm_pos[1] = p.y;
}
function getReverseGeocodingData(lat, lng) {
    var latlng = new google.maps.LatLng(lat, lng);
    // This is making the Geocode request
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({'latLng': latlng}, function(results, status) {
        // This is checking to see if the Geoeode Status is OK before proceeding
        if (status == google.maps.GeocoderStatus.OK) {
            geocoded_address = (results[0].formatted_address);
            var anno2d_address = document.getElementById("anno2d_address");
            anno2d_address.innerHTML = geocoded_address;
            if (geocoded_address.length > 110)
                anno2d_address.style.height = "36px";
            else if (geocoded_address.length > 55)
                anno2d_address.style.height = "24px";
            else
                anno2d_address.style.height = "12px";
            geocode_attempt = true;
        } else if (status === google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {
            if (geocode_attempt == false) {
                setTimeout(function() {
                    getReverseGeocodingData(lat, lng);
                }, 200);
            }
        }
    });
}
function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds) {
            break;
        }
    }
}

function validateURL(value) {
    return /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(value);
}

$(function() {
    //$( "#datetime" ).datetimepicker();
    $("#datetime").datetimepicker({
        dateFormat: $.datepicker.RFC_2822,
        timeFormat: 'hh:mm z',
        showTimezone: true
    });
});

$(document).ready(function() {

    $("#anno_radio_on").click(function() {
        if ($(this).is(':checked')) {
            anno_mode = true;
        }
    });
    $("#anno_radio_off").click(function() {
        if ($(this).is(':checked')) {
            anno_mode = false;
        }
    });
});

//Kim:
$(document).on("click", "#annotations tbody.header", function(e) {
//highlight the row
    $('#annotations tr').css('background-color', 'white');
    $(this).children('tr').css('background-color', '#aaf');
    $(this).next("tbody.data").slideToggle(200);

    var cur_anno = $(this).children('tr').attr('id');
    cur_anno_idx = cur_anno.match(/\d+$/)[0];

    for (var i = 1, n = annos.length; i <= n; i++)
    {

        if (i != cur_anno_idx) {
            annos2D[i].style.border = "yellow solid 1px";
            annos2D[i].innerHTML = "A:" + i;
            annos2D[i].style.backgroundColor = "blue";
            annos2D[i].style.width = "25px";
            annos2D[i].style.height = "12px";
        }
        else {
            //If Image URL is valide, then append the image on the annotation position
            annos2D[i].style.border = "orange solid 2px";
            cur_image_url = $(this).next("tbody.data").find("a").attr("href");

            if (validateURL(cur_image_url)) {

                annos2D[i].style.backgroundColor = "white";
                cur_span_idx = cur_anno_idx;

                var title = "A:" + i;
                href_img_id = "img_" + i;
                var img = '<div id ="' + href_img_id + '" data-url="' + cur_image_url + '"><img src="' + cur_image_url + '" title ="' + title + '" height="42" width="42"></div>';
                annos2D[i].innerHTML = img;
                annos2D[i].style.width = "42px";
                annos2D[i].style.height = "42px";
            }
        }
    }
});



$(document).on("click", "span", function(e) {
    //e.preventDefault(); //should be commented due to 'a href' function....
    var cur_span = $(this).attr('id');
    cur_span_idx = cur_span.match(/\d+$/)[0];

    for (var i = 1, n = annos.length; i <= n; i++)
    {
        if (i != cur_span_idx) {
            annos2D[i].style.border = "yellow solid 1px";
            ///
            annos2D[i].innerHTML = "A:" + i;
            annos2D[i].style.backgroundColor = "blue";
            annos2D[i].style.width = "25px";
            annos2D[i].style.height = "12px";
        }
        else {
            annos2D[i].style.border = "orange solid 2px";
            ////trouble below...
            $('#annotations tr').css('background-color', 'white');
            $('#annotations > tbody.header').each(function() { //KJS:  Working well!! '>' needed!
                var trid = $(this).children('tr').attr('id'); // table row ID
                //alert("tr id:" + trid);
                var cur_tr_idx = trid.match(/\d+$/)[0];
                if (cur_tr_idx == cur_span_idx) {
                    $(this).children('tr').css('background-color', '#aaf');
                    cur_anno_idx = cur_span_idx;
                    // show image
                    cur_image_url = $(this).next("tbody.data").find("a").attr("href");

                    if (validateURL(cur_image_url)) {

                        annos2D[i].style.backgroundColor = "white";
                        cur_span_idx = cur_anno_idx;

                        var title = "A:" + i;
                        href_img_id = "img_" + i;
                        var img = '<div id ="' + href_img_id + '" data-url="' + cur_image_url + '"><img src="' + cur_image_url + '" title ="' + title + '" height="42" width="42"></div>';

                        annos2D[i].innerHTML = img;
                        annos2D[i].style.width = "42px";
                        annos2D[i].style.height = "42px";
                    }
                }

            });
        }
    }
});


$(function() {
    //Modal dialog form
    dlg_name = $("#name");
    dlg_datetime = $("#datetime");
    dlg_note = $("#note");
    dlg_url = $("#url");
    dlg_address = $("#address");
    var allFields = $([]).add(dlg_name).add(dlg_datetime).add(dlg_address).add(dlg_note).add(dlg_url);

    var tips = $(".validateTips");
    function updateTips(t) {
        tips
                .text(t)
                .addClass("ui-state-highlight");
        setTimeout(function() {
            tips.removeClass("ui-state-highlight", 1500);
        }, 500);
    }
    function checkLength(o, n, min, max) {
        if (o.val().length > max || o.val().length < min) {
            o.addClass("ui-state-error");
            updateTips("Length of " + n + " must be between " +
                    min + " and " + max + ".");
            return false;
        } else {
            return true;
        }
    }
    
    $("#dialog-form").dialog({
        autoOpen: false,
        height: 420,
        width: 350,
        modal: true,
        dialogClass: "no-close",
        open: function() {
            //alert(geocoded_address);
            $("#address").val(geocoded_address);
            $("#datetime").datepicker('setDate', new Date());
        },
        buttons: {
            "Create an annotation": function() {
                var bValid = true;
                allFields.removeClass("ui-state-error");
                bValid = bValid && checkLength(dlg_name, "User Name", 3, 16);
                /* bValid = bValid && checkLength( datetime, "datetime", 6, 32 );
                 */
                var anno_link = '<a id ="' + annoCnt + '" href="' + dlg_url.val() + '" target="_blank"> Link </a>';
                if (!validateURL(dlg_url.val())) {
                    anno_link = dlg_url.val();
                }

                if (bValid) {
                    $("#annotations").append("<tbody class='header'>" +
                            "<tr id = anno_" + annoCnt + ">" +
                            "<td> <b>A:" + annoCnt + "</b></td></tr>" +
                            "</tbody>" +
                            "<tbody class='data'>" +
                            "<tr><td> User Name:" + dlg_name.val() + "</td></tr>" +
                            "<tr><td> Date&Time" + dlg_datetime.val() + "</td></tr>" +
                            "<tr><td> Address" + dlg_address.val() + "</td></tr>" +
                            "<tr><td> Note" + dlg_note.val() + "</td></tr>" +
                            "<tr><td> Image Link" + anno_link + "</td></tr>" +
                            "</tbody>");
                    //ToDo: Mar. 7. 2014. Assign the annotation values into global variables
                    //Insert annotations into database
                    var index = 'A:' + annoCnt;
                    var name = dlg_name.val();
                    var user_datetime = dlg_datetime.val();
                    var pos_x = hit_pos[0];
                    var pos_y = hit_pos[1];
                    var pos_z = hit_pos[2];
                    if (pos_z < 0)
                        pos_z = pos_z * (-1); //for Database, make the value positive
                    var address = dlg_address.val();
                    var note = dlg_note.val();
                    var link = anno_link;
                    
                    var url = 'http://metagrid2.sv.vt.edu:8080/scripts/geoserver_anno_db_insert.php';
                    var success = function(data) {
                        ShowStatus(true, 'Data successfully inserted!', 2000);
                    }
                    var failure = function(data)
                    {
                        ShowStatus(true, 'Insert Failed with unknown Error', 2000);
                    }
                    var error = function(jqXHR, textStatus, errorThrown) {
                        var err = eval(jqXHR.responseText);
                        alert(err.Message);
                    }

                    $.ajax({
                        type: "POST",
                        url: url,
                        data: {index: index, name: name, user_datetime: user_datetime, pos_x: pos_x, pos_y: pos_y, pos_z: pos_z, address: address, note: note, link: link},
                        success: success,
                        failure: failure,
                        error: error
                    });
////////////////////
                    $(this).dialog("close");
                }
            },
            Cancel: function() {
                $(this).dialog("close");
                var ot = document.getElementById('scene');
                ot.removeChild(annos[annoCnt]);
                document.body.removeChild(annos2D[annoCnt]);
                annoCnt = annoCnt - 1;
            }
        },
        close: function() {
            allFields.val("").removeClass("ui-state-error");
        }
    });
});
//Flash message after inserting an annotation into the table
function ShowStatus(saveMessage, message, timeInMilliseconds)
{
    var errorMessage = $("#status");
    if (saveMessage)
    {
        errorMessage.show();
        var myInterval = window.setInterval(function()
        {
            message = message + '...';
            errorMessage.text(message);
            errorMessage.show();
        }, 600);
        window.setTimeout(function()
        {
            clearInterval(myInterval);
            errorMessage.hide();
        }, timeInMilliseconds);
    }
    else
    {
        errorMessage.text(message);
        errorMessage.show();
        window.setTimeout('$("#status").hide()', timeInMilliseconds);
    }
    ;
}

function Create2DArray(max_num) {
    var arr = new Array(max_num);
    for (var i = 0; i < max_num; i++)
        arr[i] = new Array(3);

    return arr;
}

function showAnnotations() {

    var url = 'http://metagrid2.sv.vt.edu:8080/scripts/geoserver_anno_db_select.php';

    var success = function(data) {
//alert("showAnnotations() success called")  ;
        var json = $.parseJSON(data);
        //$('#result').text("");
        if (json.count > 0) {
            annoCnt = json.count;
            for (var i = 0; i < json.count; i++) {
                var idx_string = json.rows[i].index;
                var idx_number = parseInt(idx_string.match(/[0-9]+/)[0], 10);  //parseInt(json.rows[i].index, 10);

                $( "#annotations" ).append( "<tbody class='header'>" +
                "<tr id = anno_" + idx_number + ">" +
                "<td> <b>A:"+ idx_number + "</b></td></tr>" +
                "</tbody>" +
                "<tbody class='data'>"  +
                "<tr><td> <b>User Name: </b>" + json.rows[i].name + "</td></tr>" +
                "<tr><td> <b>Date&Time: </b>" + json.rows[i].datetime + "</td></tr>" +
                "<tr><td> <b>Address: </b>" + json.rows[i].address+ "</td></tr>" +
                "<tr><td> <b>Note: </b>" + json.rows[i].note + "</td></tr>" +
                "<tr><td> <b>Image Link: </b>" + json.rows[i].link + "</td></tr>" +
                "</tbody>" );
                //alert(json.rows[i].pos_2d);
                var pos_tmp = json.rows[i].pos_2d.split(' ', 2);
                var t_pos_x = parseFloat(pos_tmp[0]);
                var t_pos_z = parseFloat(pos_tmp[1]);
                if (t_pos_z > 0)
                    t_pos_z = t_pos_z * (-1); //for X3D, make the value negative...North-direction
                var t_pos_y = parseFloat(json.rows[i].pos_height);
                //Add the cone marker and annotation textbox
                var t = document.createElement('Transform');
                var cone_id = "placemarker" + idx_number;
                t.setAttribute("id", cone_id)
                t.setAttribute("scale", "10 30 10");
                t.setAttribute('rotation', '1 0 0 3.14');
                var cone_y = t_pos_y + 20;
                var trans_str = t_pos_x + ' ' + cone_y + ' ' + t_pos_z;
                t.setAttribute('translation', trans_str);

                var s = document.createElement('Shape');
                s.setAttribute("isPickable", "false");
                t.appendChild(s);
                var b = document.createElement('Cone');
                s.appendChild(b);

                var a = document.createElement('Appearance');
                var m = document.createElement('Material');
                m.setAttribute("diffuseColor", "0.1 0.1 0.1");
                m.setAttribute("transparency", "0");
                a.appendChild(m);
                s.appendChild(a);

                var ot = document.getElementById('scene');
                ot.appendChild(t);
                annos[idx_number] = t;

                // obtain corresponding 2d position on page for displaying 2d markers
                var pos2d = runtime.calcPagePos(t_pos_x, t_pos_y, t_pos_z);
                var spantag = document.createElement('span');
                spantag.id = "textbox" + idx_number;
                spantag.className = "dynamicSpan";
                spantag.style.left = (pos2d[0] + 1) + "px";
                spantag.style.top = (pos2d[1] + 1) + "px";
                spantag.innerHTML = 'A:' + idx_number;
                document.body.appendChild(spantag);
                annos2D[idx_number] = spantag;
                annos2D[idx_number].visibility = "visible";
                annos2D[idx_number].style.display = "";
                if(!zoomed) {
                    if (pos2d[0] + 25 >= 800 || pos2d[0] < 5 || pos2d[1] < 35 || pos2d[1] + 12 >= 500) {
                        annos2D[idx_number].visibility = "hidden";
                        annos2D[idx_number].style.display = "none";
                    }
                }
            } // for
        } //if
    } //success function
    var error = function(jqXHR, textStatus, errorThrown) {
        //alert(errorThrown);
        var err = eval(jqXHR.responseText);
        alert(err.Message);
    }
    $.ajax({
        type: "POST",
        url: url,
        data: {bbox_min_x: min_x, bbox_min_y: min_y, bbox_max_x: max_x, bbox_max_y: max_y},
        success: success,
        error: error
    });
}
function iframe_init() {
    zoomed = false;

    var min_point = new Proj4js.Point(init_min_x, init_min_y)
    var max_point = new Proj4js.Point(init_max_x, init_max_y)
    //Original projected points with EPSG:26917
    min_x = min_point.x;
    min_y = min_point.y;
    max_x = max_point.x;
    max_y = max_point.y;

    Proj4js.transform(source, for_OSM, min_point);
    Proj4js.transform(source, for_OSM, max_point);

    //extent_4326 = new OpenLayers.Bounds(min_point.x, min_point.y, max_point.x, max_point.y);
    extent_900913 = new OpenLayers.Bounds(min_point.x, min_point.y, max_point.x, max_point.y);

    document.getElementById('MinX').value = min_x;
    document.getElementById('MinY').value = min_y;
    document.getElementById('MaxX').value = max_x;
    document.getElementById('MaxY').value = max_y;

    document.getElementById('requested_url_text').value = "http://cgit03.cc.vt.edu:8080/geoserver_w3ds/scripts/w3ds.htm?version=0.4&service=w3ds&request=GetScene&format=text/html&crs=EPSG:26917&layers=x3d_lods:montgomery_terrain_geo_locations,x3d_lods:building_geo_locations&boundingbox=" + min_x.toString() + "," + min_y.toString() + "," + max_x.toString() + "," + max_y.toString();

    var mapOptions = {
        //div: "map",
        projection: new OpenLayers.Projection("EPSG:900913"),
        units: "m",
        tileSize: new OpenLayers.Size(256, 256)
    };

    map = new OpenLayers.Map('map', mapOptions);
    var osm = new OpenLayers.Layer.OSM();

    map.addLayer(osm);

    map.zoomToExtent(extent_900913);
    //map.addControl(new OpenLayers.Control.LayerSwitcher());

    map.events.register("move", map, function() {
     //Lat Lon extents
         ll_min_x = map.getExtent().left;
         ll_min_y = map.getExtent().bottom;
         ll_max_x = map.getExtent().right;
         ll_max_y = map.getExtent().top;

         //Transform with Proj4js
         var move_min_point =  new Proj4js.Point(ll_min_x,ll_min_y)
         var move_max_point =  new Proj4js.Point(ll_max_x,ll_max_y)

         Proj4js.transform(for_OSM, source, move_min_point);
         Proj4js.transform(for_OSM, source, move_max_point);

         extent_26917 = new OpenLayers.Bounds(move_min_point.x,move_min_point.y,move_max_point.x,move_max_point.y);

         document.getElementById('requested_url_text').value = "http://cgit03.cc.vt.edu:8080/geoserver_w3ds/scripts/w3ds.htm?version=0.4&service=w3ds&request=GetScene&format=text/html&crs=EPSG:26917&layers=x3d_lods:montgomery_terrain_geo_locations,x3d_lods:building_geo_locations&boundingbox=" + extent_26917.left + "," + extent_26917.bottom + "," + extent_26917.right + "," + extent_26917.top;
         min_x =extent_26917.left;
         max_x =extent_26917.right;
         min_y =extent_26917.bottom;
         max_y =extent_26917.top;

        document.getElementById('MinX').value = min_x;
        document.getElementById('MinY').value = min_y;
        document.getElementById('MaxX').value = max_x;
        document.getElementById('MaxY').value = max_y;

    });

    document.getElementById('map').className = "smallmap";
    //Fill out initial value

    //document.getElementById('map').style.display = "none";

    gotoURL();
}
function init()
{
    $element = document.getElementById('element');
    runtime = $element.runtime;
    runtime.showAll();

    anno_mode = false;
    document.getElementById('anno_radio_off').checked = true;

    var anno_mode_element = document.getElementById('annotation_mode');
    anno_mode_element.className = "annotation_mode";

//hide the cone marker
    var t = document.getElementById('bar');
    t.setAttribute('render', "false");

    //Get the existing annotations
    showAnnotations();

//Add Export X3D menu
    var a = document.createElement("a");
    var parent = document.getElementById('element');
    parent.appendChild(a);
  
    a.download = "geoserver_terrain.x3d";
    var html = "<x3d> <Scene>" +document.getElementById("scene").innerHTML;
    html += "</Scene> </x3d>";

    a.href = "data:text/xml," + html;
    a.innerHTML = "<span style='position: absolute; background-color: black; color: yellow; border: yellow solid 1px; padding: 6px; margin: 2px; width: 80px;  height: 12px; opacity: 0.8; text_align: center; top: 2px; right: 20px;'> EXPORT X3D          </span>";
///
    //document.getElementById('layergroup').addEventListener("mousemove", updateStuff, true);
    document.getElementsByTagName('Viewpoint').addEventListener('viewpointChanged', viewFunc, false);
    //viewFunc(null);


}
// whenever viewpoint changes, the 2d positions need to be updated
function viewFunc(evt)
{
    if (evt)
    {
        var geoPoint = evt.hitPnt;
        var view_pos = evt.position;
        var view_rot = evt.orientation;
    }
    var anno = document.getElementById("anno2d");
    if (geoPoint[2] < 0)
        geoPoint[2] = geoPoint[2] * (-1);
    anno.innerHTML = "(" + geoPoint[0].toFixed(2) + ", " + geoPoint[1].toFixed(2) + ", " + geoPoint[2].toFixed(2) + ")";

    var source = new Proj4js.Proj('EPSG:26917');
    var dest = new Proj4js.Proj('EPSG:4326');
    var utm_pos = new Array(2);
    utm_pos[0] = geoPoint[0];
    utm_pos[1] = geoPoint[2];
    if (utm_pos[1] < 0)
        utm_pos[1] = utm_pos[1] * (-1);

    UTM_LatLon(utm_pos, source, dest);

    var anno2d_latlon = document.getElementById("anno2d_latlon");
    anno2d_latlon.innerHTML = "(" + utm_pos[1].toFixed(4) + ", " + utm_pos[0].toFixed(4) + ")";

    if (geocode_attempt) {
        geocode_attempt = false;
        display_geocoded_address(utm_pos);
    }

    for (var i = 1, n = annos.length; i <= n; i++)
    {
        var trans = x3dom.fields.SFVec3f.parse(annos[i].getAttribute("translation"));
        var pos2d = runtime.calcPagePos(trans.x, trans.y, trans.z);

        annos2D[i].style.left = pos2d[0] + "px";
        annos2D[i].style.top = pos2d[1] + "px";
        annos2D[i].visibility = "visible";
        if(!zoomed) {
            if (pos2d[0] + 25 >= 800 || pos2d[0] < 5 || pos2d[1] < 35 || pos2d[1] + 12 >= 500) {
                //annos2D[i].visibility = "hidden";
                annos2D[i].style.display = "none";
            } else {
                annos2D[i].style.display = "";
            }
        }
    }
}
;

function display_geocoded_address(utm_pos)
{
    getReverseGeocodingData(utm_pos[1], utm_pos[0]);
}
// update position and orientation of line according to picked position and surface direction
function updateStuff(event)
{
    var norm = new x3dom.fields.SFVec3f(event.normalX, event.normalY, event.normalZ);
    var qDir = x3dom.fields.Quaternion.rotateFromTo(new x3dom.fields.SFVec3f(0, -1, 0), norm);
    var rot = qDir.toAxisAngle();
    var pos = event.hitPnt;

    var t = document.getElementById('bar');
    t.setAttribute('rotation', rot[0].x + ' ' + rot[0].y + ' ' + rot[0].z + ' ' + rot[1]);
    var con_y = pos[1] + 20;
    t.setAttribute('translation', pos[0] + ' ' + con_y + ' ' + pos[2]);

//The cone is shown only when the annotation mode is on.
    if (anno_mode)
        t.setAttribute('render', "true");
    else
        t.setAttribute('render', "false");
    //var pos2d = runtime.calcPagePos(event.worldX, event.worldY, event.worldZ);
    var anno = document.getElementById("anno2d");
    if (pos[2] < 0)
        pos[2] = pos[2] * (-1);
    anno.innerHTML = "(" + pos[0].toFixed(2) + ", " + pos[1].toFixed(2) + ", " + pos[2].toFixed(2) + ")";

    var source = new Proj4js.Proj('EPSG:26917');
    var dest = new Proj4js.Proj('EPSG:4326');
    var utm_pos = new Array(2);
    utm_pos[0] = pos[0];
    utm_pos[1] = pos[2];
    UTM_LatLon(utm_pos, source, dest);

    var anno2d_latlon = document.getElementById("anno2d_latlon");
    anno2d_latlon.innerHTML = "(" + utm_pos[1].toFixed(4) + ", " + utm_pos[0].toFixed(4) + ")";

    if (geocode_attempt) {
        geocode_attempt = false;
        display_geocoded_address(utm_pos);
    }
    //change the world-pos of each text box
    for (var i = 1; i <= annoCnt; i++)
    {
        var trans = x3dom.fields.SFVec3f.parse(annos[i].getAttribute("translation"));
        var pos2d = runtime.calcPagePos(trans.x, trans.y, trans.z);

        annos2D[i].style.left = pos2d[0] + "px";
        annos2D[i].style.top = pos2d[1] + "px";
        annos2D[i].visibility = "visible";
        if(!zoomed) {
            if (pos2d[0] + 25 >= 800 || pos2d[0] < 5 || pos2d[1] < 35 || pos2d[1] + 12 >= 500) {
                //annos2D[i].visibility = "hidden";
                annos2D[i].style.display = "none";
            } else {
                annos2D[i].style.display = "";
            }
        }
    }
    var div_section = document.getElementById("cur_viewpoint");

    var viewpoint = $element.runtime.getActiveBindable("Viewpoint");
    var cur_pos = t.getAttribute("translation");
    var cur_pos_tmp = cur_pos.split(' ', 3);
    var t_pos_x = parseFloat(cur_pos_tmp[0]);
    var t_pos_y = parseFloat(cur_pos_tmp[1]);
    var t_pos_z = parseFloat(cur_pos_tmp[2]);
    div_section.innerHTML = t_pos_x + ', ' + t_pos_y + ', ' + t_pos_z;
    cur_bar_pos = cur_pos;
}
function addNode(event)
{
    // var rightclick = false;
    var e = event;
    // if (e.which) rightclick = (e.which == 3);
    //    else if (e.button) rightclick = (e.button == 2);
    // rotate such that cone points to click point
    var norm = new x3dom.fields.SFVec3f(event.normalX, event.normalY, event.normalZ);
    var qDir = x3dom.fields.Quaternion.rotateFromTo(new x3dom.fields.SFVec3f(0, -1, 0), norm);
    var rot = qDir.toAxisAngle();
    var pos = event.hitPnt;
    hit_pos[0] = pos[0];
    hit_pos[1] = pos[1];
    hit_pos[2] = pos[2];

    if (anno_mode)
    {
        annoCnt = annoCnt + 1;
        //alert("next annoCnt:" + annoCnt);
        var t = document.createElement('Transform');
        var cone_id = "placemarker" + annoCnt;
        t.setAttribute("id", cone_id)
        t.setAttribute("scale", "10 30 10");
        t.setAttribute('rotation', rot[0].x + ' ' + rot[0].y + ' ' + rot[0].z + ' ' + rot[1]);
        var con_y = pos[1] + 20;
        t.setAttribute('translation', pos[0] + ' ' + con_y + ' ' + pos[2]);

        var s = document.createElement('Shape');
        s.setAttribute("isPickable", "false");
        t.appendChild(s);
        var b = document.createElement('Cone');
        s.appendChild(b);

        var a = document.createElement('Appearance');
        var m = document.createElement('Material');
        m.setAttribute("diffuseColor", "0.1 0.1 0.1");
        m.setAttribute("transparency", "0");
        a.appendChild(m);
        s.appendChild(a);

        var ot = document.getElementById('scene');
        ot.appendChild(t);
        annos[annoCnt] = t;

        // obtain corresponding 2d position on page for displaying 2d markers
        var pos2d = runtime.calcPagePos(event.worldX, event.worldY, event.worldZ);
        var spantag = document.createElement('span');
        spantag.id = "textbox" + annoCnt;
        spantag.className = "dynamicSpan";
        spantag.style.left = (pos2d[0] + 1) + "px";
        spantag.style.top = (pos2d[1] + 1) + "px";
        spantag.innerHTML = 'A:' + annoCnt;
        document.body.appendChild(spantag);
        annos2D[annoCnt] = spantag;

        //get the address of the current position
        var geoPoint = e.hitPnt;

        var source = new Proj4js.Proj('EPSG:26917');
        var dest = new Proj4js.Proj('EPSG:4326');
        var utm_pos = new Array(2);
        utm_pos[0] = geoPoint[0];
        utm_pos[1] = geoPoint[2];
        if (utm_pos[1] < 0)
            utm_pos[1] = utm_pos[1] * (-1);

        UTM_LatLon(utm_pos, source, dest);

        if (geocode_attempt) {
            geocode_attempt = false;
            display_geocoded_address(utm_pos);
        }

        $("#dialog-form").dialog("open");
    }
    else {
        if (nav_change) {
            var viewpoint = $element.runtime.getActiveBindable("Viewpoint");
            viewpoint.setAttribute('centerOfRotation', pos);
            nav_change = false;
        }
    }
}

function turntable_nav(param1, param2, param3, param4) {
    var str = param1.toFixed(1) + '  ' + param2.toFixed(1) + '  ' + param3.toFixed(1) + ' ' + param4.toFixed(1);
    var nav_mode = $element.runtime.getActiveBindable("NavigationInfo");
    nav_mode.setAttribute('type', 'turntable');
    nav_mode.setAttribute('typeParams', str);
}


function showManualBBoxExtents() {
    document.getElementById('map').style.display = "none";

    $('#dataDump').text("");

    $('#dataDump').append("<br>");
    $('#dataDump').append('Min_X:<input id="MinX" size="10" value=""></input>');
    $('#dataDump').append('Min_Y:<input id="MinY" size="10" value=""></input>');
    $('#dataDump').append("<br>");
    $('#dataDump').append('Max_X:<input id="MaxX" size="10" value=""></input>');
    $('#dataDump').append('Max_Y:<input id="MaxY" size="10" value=""></input>');
    $('#dataDump').append("<br>");
    document.getElementById('MinX').value = min_x;
    document.getElementById('MinY').value = min_y;
    document.getElementById('MaxX').value = max_x;
    document.getElementById('MaxY').value = max_y;

    $('#dataDump').append('<button class="generalButton" type="button" onclick="ResetBounds()">Reset BBox</button>');


    $('#dataDump').append('<button class="generalButton" type="button" onclick="GetRequestURL()">Apply BBox to the Requested URL</button>');
    $('#dataDump').append("<br>");
    $('#dataDump').append("<br>");

    document.getElementById('requested_url_text').value = "http://cgit03.cc.vt.edu:8080/geoserver_w3ds/scripts/w3ds.htm?version=0.4&service=w3ds&request=GetScene&format=text/html&crs=EPSG:26917&layers=x3d_lods:montgomery_terrain_geo_locations,x3d_lods:building_geo_locations&boundingbox=" + min_x.toString() + "," + min_y.toString() + "," + max_x.toString() + "," + max_y.toString();
}


function gotoURL() {

    var newURL = document.url2go.go.value;
    //newURL = "http://metagrid2.sv.vt.edu:8080/scripts/iframe_x3dom.html";
    document.getElementById('X3DOM_iframe').setAttribute('src', newURL);

    // min_x = document.getElementById('MinX').value;
    // min_y = document.getElementById('MinY').value;
    // max_x = document.getElementById('MaxX').value;
    // max_y = document.getElementById('MaxY').value;

    var min_point = new Proj4js.Point(min_x, min_y)
    var max_point = new Proj4js.Point(max_x, max_y)
  
    Proj4js.transform(source, for_OSM, min_point);
    Proj4js.transform(source, for_OSM, max_point);
    
    extent_900913 = new OpenLayers.Bounds(min_point.x, min_point.y, max_point.x, max_point.y);
    map.zoomToExtent(extent_900913);


}
function ResetBounds() { 
    min_x = init_min_x;
    min_y = init_min_y;
    max_x = init_max_x;
    max_y = init_max_y;

    var min_point = new Proj4js.Point(min_x, min_y)
    var max_point = new Proj4js.Point(max_x, max_y)
  
    Proj4js.transform(source, for_OSM, min_point);
    Proj4js.transform(source, for_OSM, max_point);
    
    extent_900913 = new OpenLayers.Bounds(min_point.x, min_point.y, max_point.x, max_point.y);
    map.zoomToExtent(extent_900913);

    document.getElementById('requested_url_text').value = "http://cgit03.cc.vt.edu:8080/geoserver_w3ds/scripts/w3ds.htm?version=0.4&service=w3ds&request=GetScene&format=text/html&crs=EPSG:26917&layers=x3d_lods:montgomery_terrain_geo_locations,x3d_lods:building_geo_locations&boundingbox=" + init_min_x.toString() + "," + init_min_y.toString() + "," + init_max_x.toString() + "," + init_max_y.toString();

    document.getElementById('MinX').value = min_x;
    document.getElementById('MinY').value = min_y;
    document.getElementById('MaxX').value = max_x;
    document.getElementById('MaxY').value = max_y;
}

function GetRequestURL() {
    min_x = document.getElementById('MinX').value;
    min_y = document.getElementById('MinY').value;
    max_x = document.getElementById('MaxX').value;
    max_y = document.getElementById('MaxY').value;
    document.getElementById('requested_url_text').value = "http://cgit03.cc.vt.edu:8080/geoserver_w3ds/scripts/w3ds.htm?version=0.4&service=w3ds&request=GetScene&format=text/html&crs=EPSG:26917&layers=x3d_lods:montgomery_terrain_geo_locations,x3d_lods:building_geo_locations&boundingbox=" + min_x.toString() + "," + min_y.toString() + "," + max_x.toString() + "," + max_y.toString();

}
function showOpenlayerMap() {

    document.getElementById('requested_url_text').value = "http://cgit03.cc.vt.edu:8080/geoserver_w3ds/scripts/w3ds.htm?version=0.4&service=w3ds&request=GetScene&format=text/html&crs=EPSG:26917&layers=x3d_lods:montgomery_terrain_geo_locations,x3d_lods:building_geo_locations&boundingbox=" + min_x.toString() + "," + min_y.toString() + "," + max_x.toString() + "," + max_y.toString();

    document.getElementById('map').style.display = "";
    document.getElementById('map').className = "smallmap";
    $('#dataDump').append('<button class="generalButton" type="button" onclick="ResetBounds()">Reset BBox</button>');

}

function bboxSelectHandler()
{
    var select = document.getElementById('bboxType');
    var value = select.options[select.selectedIndex].value;

    $('#dataDump').text("");
    if (value == "extents")
    {
        showManualBBoxExtents();
    }
    else if (value == "openlayermap")
    {
        showOpenlayerMap();
    }
    else
    {
        $('#dataDump').text("Please select a set of data.");
    }
}

function optionBBoxHandler(option)
{
    $('#bboxoptions').text("");
}

function applyMapExtent() {
    ll_min_x = map.getExtent().left;
    ll_min_y = map.getExtent().bottom;
    ll_max_x = map.getExtent().right;
    ll_max_y = map.getExtent().top;

    //Transform with Proj4js
    var move_min_point = new Proj4js.Point(ll_min_x, ll_min_y)
    var move_max_point = new Proj4js.Point(ll_max_x, ll_max_y)

    Proj4js.transform(for_OSM, source, move_min_point);
    Proj4js.transform(for_OSM, source, move_max_point);

    extent_26917 = new OpenLayers.Bounds(move_min_point.x, move_min_point.y, move_max_point.x, move_max_point.y);
    min_x =extent_26917.left;
    max_x =extent_26917.right;
    min_y =extent_26917.bottom;
    max_y =extent_26917.top;

    document.getElementById('requested_url_text').value = "http://metagrid2.sv.vt.edu:8080/geoserver/x3d_lods/w3ds?version=0.4&service=w3ds&request=GetScene&format=text/html&crs=EPSG:26917&layers=x3d_lods:montgomery_terrain_geo_locations,x3d_lods:building_geo_locations&boundingbox=" + extent_26917.left + "," + extent_26917.bottom + "," + extent_26917.right + "," + extent_26917.top;
}
